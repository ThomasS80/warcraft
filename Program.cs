﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monstres;

namespace WarcraftConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Peasant Humain_Jeanneymar, Humain_Patoche;
            Peon Orc_Jeanminou, Orc_Paulo;
            Humain_Jeanneymar = new Peasant("Jeanneymar", "Peasant", "Human", "Alliance", 30, 0);
            Humain_Patoche = new Peasant("Patoche", "Peasant", "Human", "Alliance", 30, 0);
            Console.WriteLine(Humain_Jeanneymar.sayHello());
            Console.WriteLine(Humain_Patoche.talk());
            Console.WriteLine(Humain_Jeanneymar.talkToPeasant(Humain_Patoche));
            Orc_Jeanminou = new Peon("Jeanminou ", "Peon", "Orc", "Horde", 30, 0);
            Orc_Paulo = new Peon("Paulo", "Peon", "Orc", "Horde", 30, 0);
            Console.WriteLine(Orc_Jeanminou.sayHello());
            Console.WriteLine(Orc_Paulo.grunt());
            Console.WriteLine(Orc_Paulo.talkToPeon(Orc_Jeanminou));
            Console.ReadKey();

        }
    }
}
