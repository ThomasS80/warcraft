﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarcraftConsole;
using Monstres;

namespace WarcraftTest
{
    [TestClass]
    public class WarcraftProgTest
    {
        [TestMethod]
        public void ConstructorPeasantTest()
        {
            Peon Orc = new Peon("Jean-minou", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("Peon", Orc.type);
            Assert.AreEqual("Orc", Orc.race);
            Assert.AreEqual("Horde", Orc.faction);
            Assert.AreEqual(30, Orc.hit_points);
            Assert.AreEqual(0, Orc.armor);
        }
        [TestMethod]
        public void ConstructorHumanTest()
        {
            Peasant Humain = new Peasant("Jean-neymar ", "Peasant", "Humain", "alliance", 30, 0);
            Assert.AreEqual("Peasant", Humain.type);
            Assert.AreEqual("Humain", Humain.race);
            Assert.AreEqual("alliance", Humain.faction);
            Assert.AreEqual(30, Humain.hit_points);
            Assert.AreEqual(0, Humain.armor);
        }
        [TestMethod]
        public void peonSayHelloTest()
        {
            Peon Orc = new Peon("Jean-minou", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("Je suis un Peon , de la race des Orc. Je fais partie de l'Horde. j'ai 30 points de vie et 0 d'armure", Orc.sayHello());
        }
        [TestMethod]
        public void peasantSayHelloTest()
        {
            Peasant Humain = new Peasant("Jean-neymar", "Peasant", "Humain", "Alliance", 30, 0);
            Assert.AreEqual("Je suis un Peasant , de la race des Humain. Je fais partie des Alliance. j'ai 30 points de vie et 0 d'armure", Humain.sayHello());
        }
        [TestMethod]
        public void GruntTest()
        {
            Peon Orc = new Peon("Jean-minou", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("ROOOOOAAAARRRR", Orc.grunt());
        }
        [TestMethod]
        public void talk()
        {
            Peasant Humain = new Peasant("Jean-neymar", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("T ki toi tu veux koi", Humain.talk());
        }
        [TestMethod]
        public void talkToPeasanTest()
        {
            Peasant Humain_Jeanneymar = new Peasant("Jean-neymar", "Peon", "Orc", "Horde", 30, 0);
            Peasant Humain_patoche = new Peasant("Patoche", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("Wsh tu veux quoi Patoche", Humain_Jeanneymar.talkToPeasant(Humain_patoche));
        }
        [TestMethod]
        public void talkToPeon()
        {
            Peon Orc_Jeanminou = new Peon("Jean-minou", "Peon", "Orc", "Horde", 30, 0);
            Peon Orc_Paulo = new Peon("Paulo", "Peon", "Orc", "Horde", 30, 0);
            Assert.AreEqual("J'vais te deglinguer ti va rien konprendre Paulo", Orc_Jeanminou.talkToPeon(Orc_Paulo));
        }
    }

}
